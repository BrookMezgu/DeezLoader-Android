package com.dt3264.deezloader

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.widget.ContentLoadingProgressBar
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.socket.client.Socket
import okhttp3.*
import java.io.*
import java.util.*
import kotlin.system.exitProcess

// Force reloading node data every time the app starts, for debugging only
const val forceReloadNodeData = false

// Build number of the running app
const val clientBuildNumber = BuildConfig.VERSION_CODE
const val clientVersion = BuildConfig.VERSION_NAME

const val telegramURL = "https://t.me/joinchat/Ed1JxEfoci-dp-BWGRdVLg"
const val serverURL = "http://localhost:1730"
const val serviceChannelID = "${BuildConfig.APPLICATION_ID}.service"
const val downloadChannelID = "${BuildConfig.APPLICATION_ID}.downloads"

// Notification ID for the NodeJS service
const val serviceNotificationID = 10

// Variables to hold the last app version that ran and whether the landing page has been displayed
var lastCompile = 0
var landingDone = false

// Application-wide shared preferences
lateinit var sharedPreferences: SharedPreferences

// Variable that holds whether the service is running or not
var isServiceRunning = false

// An object to supply an interface between NodeJS mobile and the wrapper app
object NodeNative {
    // Loads native libraries upon application startup
    init {
        System.loadLibrary("native-lib")
        System.loadLibrary("node")
    }

    // Native method that starts the NodeJS mobile server with the supplied arguments
    @JvmStatic
    external fun startNodeServer(arguments: Array<String>): Int

    // Observer which receives and handles the data
    lateinit var mainObserver: Observer<Message>

    // Rx2 components
    lateinit var serviceObserver: Observer<Message>
    lateinit var serviceSocket: Socket
}

class MainActivity : AppCompatActivity() {
    private var serviceCheckTick = Timer()
    private lateinit var context: Context

    // UI elements
    private lateinit var snackbar: Snackbar
    private lateinit var mainExternalButton: AppCompatButton
    private lateinit var mainInternalButton: AppCompatButton
    private lateinit var telegramButton: AppCompatButton
    private lateinit var updateButton: AppCompatButton
    private lateinit var infoView: AppCompatTextView
    private lateinit var updateText: AppCompatTextView
    private lateinit var versionText: AppCompatTextView
    private lateinit var progressBar: ContentLoadingProgressBar

    private var okHttpClient = OkHttpClient()
    private val _mainDataHandler = object : Observer<Message> {
        override fun onSubscribe(d: Disposable) {}

        override fun onNext(msg: Message) {
            if (msg.type == 4) {
                finishAndRemoveTask()
            }
        }

        override fun onError(e: Throwable) {}

        override fun onComplete() {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainExternalButton = findViewById(R.id.openAppExternalButton)
        mainInternalButton = findViewById(R.id.openAppInternalButton)
        telegramButton = findViewById(R.id.telegramButton)
        updateButton = findViewById(R.id.updateButton)
        infoView = findViewById(R.id.infoView)
        updateText = findViewById(R.id.updateText)
        versionText = findViewById(R.id.versionText)
        progressBar = findViewById(R.id.progressBar)

        checkPermissions()

        prepareMainObserver()

        infoView.text = getText(R.string.serverLoadingText)
        val versionString = "${getText(R.string.actualVersion)} $clientVersion"
        versionText.text = versionString

        telegramButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(telegramURL)
                }
            )
        }

        mainExternalButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(serverURL)
                }
            )
        }

        mainInternalButton.setOnClickListener {
            startActivity(
                Intent(applicationContext, BrowserActivity()::class.java).apply {
                    action = Intent.ACTION_MAIN
                    addCategory(Intent.CATEGORY_LAUNCHER)
                    addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                }
            )
        }

        snackbar = Snackbar.make(infoView, "Preparing server data", Snackbar.LENGTH_INDEFINITE)
        snackbar.show()
        checkForUpdates()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        lastCompile = sharedPreferences.getInt("lastCompile", 0)
    }

    // Main methods
    private fun checkPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), 100)
    }

    internal fun readyLandingPage() {
        context = this
        runOnUiThread {
            progressBar.visibility = View.GONE
            telegramButton.visibility = View.VISIBLE
            mainExternalButton.visibility = View.VISIBLE
            mainInternalButton.visibility = View.VISIBLE
            snackbar.dismiss()
            infoView.text = getText(R.string.serverReady)
        }
        landingDone = true
    }

    private fun checkForUpdates() {
        val request = Request.Builder().url("https://pastebin.com/raw/rEubX2Lu").build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val pasteResult: Array<String> = Objects.requireNonNull<ResponseBody>(response.body).string().split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (clientVersion != pasteResult[0].trim { it <= ' ' }) {
                    runOnUiThread {
                        val updateString = "A new version (${pasteResult[0].trim { it <= ' ' }}) is available.\n${pasteResult.drop(2).joinToString("\n- ", "\n- ")}"
                        updateText.text = updateString
                        updateButton.setOnClickListener {
                            startActivity(
                                Intent(Intent.ACTION_VIEW).apply {
                                    data = Uri.parse(pasteResult[1])
                                }
                            )
                        }
                        updateText.visibility = View.VISIBLE
                        updateButton.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun prepareMainObserver() {
        NodeNative.mainObserver = _mainDataHandler
    }

    override fun onDestroy() {
        serviceCheckTick.cancel()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (this@MainActivity.hasWindowFocus()) {
            AlertDialog.Builder(this@MainActivity).apply {
                setMessage(getString(R.string.exit_back))
                setCancelable(true)
                setPositiveButton(R.string.confirmation) { _, _ ->
                    finish()
                    exitProcess(0)
                }
                setNegativeButton(R.string.denial) { dialogInterface, _ -> dialogInterface.cancel() }
                create().apply { show() }
            }
        }
    }

    private fun saveLastUpdateTime() {
        sharedPreferences.edit().apply {
            putInt("lastCompile", clientBuildNumber)
            apply()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            100 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    serviceCheckTick.schedule(CheckDeezloaderService(), 0, 10000)
                } else {
                    // permission denied, boo!
                    Toast.makeText(this, R.string.permission,
                            Toast.LENGTH_SHORT).show()
                    this.finish()
                }
            }
        }
    }

    // Function to check whether the local assets are up to date, if not update them
    private fun checkIsApkUpdated() {
        if (forceReloadNodeData || lastCompile != clientBuildNumber) {
            //Recursively delete any existing nodejs-project.
            val nodeDir = applicationContext.filesDir.absolutePath + "/deezerLoader"
            val nodeDirReference = File(nodeDir)
            if (nodeDirReference.exists()) {
                deleteFolderRecursively(nodeDirReference)
            }

            //Copy the node project from assets into the application's data path.
            copyAssets(applicationContext.assets, "deezerLoader", nodeDir)
            saveLastUpdateTime()
        }
    }

    // Timer that periodically checks whether the service is still running, sometimes it gets killed unexpectedly
    private inner class CheckDeezloaderService : TimerTask() {
        override fun run() {
            if (!isServiceRunning) {
                Thread {
                    checkIsApkUpdated()
                    runOnUiThread { snackbar.setText(R.string.start_server) }
                    startService(
                        Intent(applicationContext, DeezloaderService()::class.java).apply {
                            action = "Deezloader Service"
                        }
                    )
                }.start()
                while (!landingDone) {
                    if (isServiceRunning) {
                        readyLandingPage()
                        break
                    }
                }
            }
        }
    }

    // Function to copy asset files from the APK to the apps internal data
    private fun copyAssets(assetManager: AssetManager, fromAssetPath: String, toPath: String) {
        try {
            val assetFiles = assetManager.list(fromAssetPath)

            if (assetFiles!!.isEmpty()) {
                File(toPath).createNewFile()
                val outFile: OutputStream = FileOutputStream(toPath)
                val inFile: InputStream = assetManager.open(fromAssetPath)
                outFile.write(inFile.readBytes())
                inFile.close()
                outFile.flush()
            } else {
                File(toPath).mkdirs()
                for (file in assetFiles) {
                    copyAssets(assetManager, "$fromAssetPath/$file", "$toPath/$file")
                }
            }
        } catch (error: Exception) {
            error.printStackTrace()
        }

    }

    // Function to delete the old asset folder and its contained files
    private fun deleteFolderRecursively(file: File) {
        try {
            for (childFile in file.listFiles()!!) {
                if (childFile.isDirectory) {
                    deleteFolderRecursively(childFile)
                } else {
                    childFile.delete()
                }
            }
            file.delete()
        } catch (error: Exception) {
            error.printStackTrace()
        }
    }
}

/*
    Type=1 -> Site ready
    Type=2 -> New folder request
    Type=3 -> New folder result
    Type=4 -> Exit app
*/
class Message(val type: Int, val message: String) {
    companion object {
        fun transmitMessage(msg: Message): Observable<Message> {
            return Observable.just(msg)
        }
    }
}